package br.com.caelum.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PrimeraLogica implements Logica {
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception{
		System.out.println("Ejecutando la logica...");
		System.out.println("Retornando al nombre de la pagina JSP...");
		return "primera-logica.jsp";
	}
}
