package br.com.caelum.mvc.logica;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.jdbc.Contact;
import br.com.caelum.jdbc.ContactDAO;

public class ListContactsLogic implements Logica {
	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		
		List<Contact> contactos = new ContactDAO().getLista();
		
		req.setAttribute("contactos", contactos);
		
		System.out.println("Mostrando lista de contactos...");
		
		return "/WEB-INF/jsp/lista-contactos.jsp";
	}
}
