package br.com.caelum.mvc.logica;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.jdbc.Contact;
import br.com.caelum.jdbc.ContactDAO;

public class RemoveContactLogic implements Logica {
	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		
		long id = Long.parseLong(req.getParameter("id"));
		
		Contact contacto = new Contact();
		contacto.setId(id);
		
		Connection conexion = (Connection) req.getAttribute("conexion");
		
		ContactDAO dao = new ContactDAO(conexion);
		dao.remueve(contacto);
		
		System.out.println("Contacto removido...");
		
		return "mvc?logica=ListContactsLogic";
	}
}
