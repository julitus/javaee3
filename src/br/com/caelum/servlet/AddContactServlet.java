package br.com.caelum.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
/*import javax.servlet.annotation.WebServlet;*/
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.jdbc.Contact;
import br.com.caelum.jdbc.ContactDAO;

/*@WebServlet("/adicionaContacto")*/
public class AddContactServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String direc = request.getParameter("direccion");
		String birth = request.getParameter("fechanac");
		Calendar dateNac = null;
		
		try{
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(birth);
			dateNac = Calendar.getInstance();
			dateNac.setTime(date);
		}catch(ParseException e){
			out.println("Error de conversion de datos");
			return;
		}
		
		Contact contacto = new Contact();
		contacto.setNombre(name);
		contacto.setDireccion(direc);	
		contacto.setEmail(email);
		contacto.setFechaNac(dateNac);
		
		ContactDAO dao = new ContactDAO();
		dao.adiciona(contacto);
		
		/*
		out.println("<html>");
        out.println("<body>");
        out.println("Contacto " + contacto.getNombre() + " agregado con exito!");
        out.println("</body>");
        out.println("</html>");
        */
		
		RequestDispatcher rd = request.getRequestDispatcher("/contacto-adiciona.jsp");
		rd.forward(request, response);
		
	}
}
