package br.com.caelum.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ContactDAO {
	
	private Connection conexion;
	
	public ContactDAO(){
		this.conexion = new ConnectionFactory().getConnection();
	}
	
	public ContactDAO(Connection conexion){
			this.conexion = conexion;
	}
	
	public void adiciona(Contact contacto){
		String sql = "insert into contactos " +
					"(name, email, direccion, fechanac) " +
					"values (?,?,?,?)";
		try{
			PreparedStatement stmt = conexion.prepareStatement(sql);
			stmt.setString(1, contacto.getNombre());
			stmt.setString(2, contacto.getEmail());
			stmt.setString(3, contacto.getDireccion());
			stmt.setDate(4, new Date(contacto.getFechaNac().getTimeInMillis()));
			
			stmt.execute();
			stmt.close();
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public void altera(Contact contacto) {
	     String sql = "update contactos set name=?, email=?, direccion=?," +
	             "fechanac=? where id=?";
	     try {
	         PreparedStatement stmt = conexion.prepareStatement(sql);
	         stmt.setString(1, contacto.getNombre());
	         stmt.setString(2, contacto.getEmail());
	         stmt.setString(3, contacto.getDireccion());
	         stmt.setDate(4, new Date(contacto.getFechaNac().getTimeInMillis()));
	         stmt.setLong(5, contacto.getId());
	         stmt.execute();
	         stmt.close();
	     } catch (SQLException e) {
	         throw new RuntimeException(e);
	     }
	}
	
	public void remueve(Contact contacto) {
	     try {
	         PreparedStatement stmt = conexion.prepareStatement("delete " + " from contactos where id=?");
	         stmt.setLong(1, contacto.getId());
	         stmt.execute();
	         stmt.close();
	     } catch (SQLException e) {
	         throw new RuntimeException(e);
	     }
	}
	
	public List<Contact> getLista(){
		try{
			List<Contact> contactos = new ArrayList<Contact>();
			PreparedStatement stmt = this.conexion.prepareStatement("select * from contactos");
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				Contact contacto = new Contact();
				contacto.setId(rs.getLong("id"));
				contacto.setNombre(rs.getString("name"));
				contacto.setEmail(rs.getString("email"));
				contacto.setDireccion(rs.getString("direccion"));
				
				Calendar data = Calendar.getInstance();
				data.setTime(rs.getDate("fechanac"));
				contacto.setFechaNac(data);
				
				contactos.add(contacto);
			}
			rs.close();
			stmt.close();
			return contactos;
		}catch (SQLException e){
			throw new RuntimeException(e);
		}
		
	}
}
