<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "java.util.*,
	br.com.caelum.jdbc.ContactDAO,
	br.com.caelum.jdbc.Contact" %>
	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
	<body>
		<!-- <table>
      	<%
      		ContactDAO dao = new ContactDAO();
      		List<Contact> contactos = dao.getLista();
      
      		for (Contact contacto : contactos ) {
      		%>
        	<tr>
	          <td><%=contacto.getNombre() %></td> 
	          <td><%=contacto.getEmail() %></td>
	          <td><%=contacto.getDireccion() %></td>
	          <td><%=contacto.getFechaNac().getTime() %></td>
	        </tr>
      		<%
      		}
     	%>
    	</table> -->
    	
    	<c:import url="/cabezera.jsp" />
    	
    	<!-- <jsp:useBean id="daoo" class="br.com.caelum.jdbc.ContactDAO" />
    	
    	<table border="1">
    		<tr>
    			<td>NOMBRE</td>
    			<td>E-MAIL</td>
    			<td>DIRECCION</td>
    			<td>FECHA NAC</td>
    		</tr>
    		<c:forEach var="contacto" items="${daoo.lista}">
    			<tr>
    				<td>${contacto.nombre}</td>
    				<td>
    					<c:if test="${not empty contacto.email}">
    						<a href="mailto:${contacto.email}">${contacto.email}</a>
    					</c:if>
    					<c:if test="${empty contacto.email}">
    						E-mail no informado
    					</c:if>
    				</td>
    				<td>${contacto.direccion}</td>
    				<td>
    					<fmt:formatDate value="${contacto.fechaNac.time}" 
    						pattern="dd/MM/yyyy" />
    				</td>
    				<td>
    					<a href="mvc?logica=RemoveContactLogic&id=${contacto.id}">Remover</a>
    				</td>
    			</tr>
    		</c:forEach>
    	</table> -->
    	
    	<table border="1">
    		<tr>
    			<td>NOMBRE</td>
    			<td>E-MAIL</td>
    			<td>DIRECCION</td>
    			<td>FECHA NAC</td>
    			<td>-Accion-</td>
    		</tr>
    		<c:forEach var="contacto" items="${contactos}">
    			<tr>
    				<td>${contacto.nombre}</td>
    				<td>
    					<c:if test="${not empty contacto.email}">
    						<a href="mailto:${contacto.email}">${contacto.email}</a>
    					</c:if>
    					<c:if test="${empty contacto.email}">
    						E-mail no informado
    					</c:if>
    				</td>
    				<td>${contacto.direccion}</td>
    				<td>
    					<fmt:formatDate value="${contacto.fechaNac.time}" 
    						pattern="dd/MM/yyyy" />
    				</td>
    				<td>
    					<a href="mvc?logica=RemoveContactLogic&id=${contacto.id}">Remover</a>
    				</td>
    			</tr>
    		</c:forEach>
    	</table>    	
    	
    	<c:import url="/pie.jsp" />
    	
	</body>
</html>