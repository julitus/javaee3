<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="caelum" %>

<html>
	
	<head>
		<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
		<script src="jquery-ui/external/jquery/jquery.js"></script>
		<script src="jquery-ui/jquery-ui.js"></script>
	</head>
	
	<body>
	
		<c:import url="/cabezera.jsp" />
		
		<h1>Adiciona Contactos</h1>
		<hr />
		<form action="adicionaContacto">
			Nombre: <input type="text" name="name" /><br />
			E-mail: <input type="text" name="email" /><br />
			Direccion: <input type="text" name="direccion" /><br />
			Fecha de Nac.: <caelum:campoData id="fechanac" /><br />
			<input type="submit" value="GUARDAR" />
		</form>
		
		<c:import url="/pie.jsp" />
		
	</body>
</html>